# file generated from go.mod using vgo2nix (https://github.com/adisbladis/vgo2nix)
[
  {
    goPackagePath = "cloud.google.com/go";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-cloud-go";
      rev = "v0.45.1";
      sha256 = "18jcrm504yxxbrkb7vxzia7awc8yr0vb0hsddwvqk15c1rgi7ivj";
    };
  }
  {
    goPackagePath = "github.com/BurntSushi/toml";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/toml";
      rev = "v0.3.1";
      sha256 = "1fjdwwfzyzllgiwydknf1pwjvy49qxfsczqx5gz3y0izs7as99j6";
    };
  }
  {
    goPackagePath = "github.com/BurntSushi/xgb";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/xgb";
      rev = "27f122750802";
      sha256 = "18lp2x8f5bljvlz0r7xn744f0c9rywjsb9ifiszqqdcpwhsa0kvj";
    };
  }
  {
    goPackagePath = "github.com/agext/levenshtein";
    fetch = {
      type = "git";
      url = "https://github.com/agext/levenshtein";
      rev = "v1.2.2";
      sha256 = "19d7q69yhcg7gl81j038rkbjz8yjb4qwnsqrmxa4zvhgzlc7d130";
    };
  }
  {
    goPackagePath = "github.com/agl/ed25519";
    fetch = {
      type = "git";
      url = "https://github.com/agl/ed25519";
      rev = "5312a6153412";
      sha256 = "1v8mhkf1m3ga5262s75vabskxvsw5rpqvi5nwhxwiv7gfk6h823i";
    };
  }
  {
    goPackagePath = "github.com/apparentlymart/go-cidr";
    fetch = {
      type = "git";
      url = "https://github.com/apparentlymart/go-cidr";
      rev = "v1.0.1";
      sha256 = "0fzvmpjyg5410za4rwbkyb2rpjvcl5sxa1a97q9jh6q633w26hj2";
    };
  }
  {
    goPackagePath = "github.com/apparentlymart/go-dump";
    fetch = {
      type = "git";
      url = "https://github.com/apparentlymart/go-dump";
      rev = "042adf3cf4a0";
      sha256 = "1yqc50v82za54j2yy3ln7jzx983scb5gbh195wb6vmvqj18q696q";
    };
  }
  {
    goPackagePath = "github.com/apparentlymart/go-textseg";
    fetch = {
      type = "git";
      url = "https://github.com/apparentlymart/go-textseg";
      rev = "v1.0.0";
      sha256 = "0n9xcyj7p5y8mbqilk9zprfyqvgm2y9f1g440wqw9dnn3s4fi1k4";
    };
  }
  {
    goPackagePath = "github.com/armon/go-radix";
    fetch = {
      type = "git";
      url = "https://github.com/armon/go-radix";
      rev = "v1.0.0";
      sha256 = "1m1k0jz9gjfrk4m7hjm7p03qmviamfgxwm2ghakqxw3hdds8v503";
    };
  }
  {
    goPackagePath = "github.com/aws/aws-sdk-go";
    fetch = {
      type = "git";
      url = "https://github.com/aws/aws-sdk-go";
      rev = "v1.25.3";
      sha256 = "1f6z5gccmrkl7y5kz6dwbbhc9xrjxmaj9zis79q2qijzmxmp7jd3";
    };
  }
  {
    goPackagePath = "github.com/bgentry/go-netrc";
    fetch = {
      type = "git";
      url = "https://github.com/bgentry/go-netrc";
      rev = "9fd32a8b3d3d";
      sha256 = "0dn2h8avgavqdzdqnph8bkhj35bx0wssczry1zdczr22xv650g1l";
    };
  }
  {
    goPackagePath = "github.com/bgentry/speakeasy";
    fetch = {
      type = "git";
      url = "https://github.com/bgentry/speakeasy";
      rev = "v0.1.0";
      sha256 = "02dfrj0wyphd3db9zn2mixqxwiz1ivnyc5xc7gkz58l5l27nzp8s";
    };
  }
  {
    goPackagePath = "github.com/cheggaaa/pb";
    fetch = {
      type = "git";
      url = "https://github.com/cheggaaa/pb";
      rev = "v1.0.27";
      sha256 = "1f995nsqgi0r4qc16nmxlbdxc536hkfwa5k9xb8ypz8d6jlkihag";
    };
  }
  {
    goPackagePath = "github.com/client9/misspell";
    fetch = {
      type = "git";
      url = "https://github.com/client9/misspell";
      rev = "v0.3.4";
      sha256 = "1vwf33wsc4la25zk9nylpbp9px3svlmldkm0bha4hp56jws4q9cs";
    };
  }
  {
    goPackagePath = "github.com/davecgh/go-spew";
    fetch = {
      type = "git";
      url = "https://github.com/davecgh/go-spew";
      rev = "v1.1.1";
      sha256 = "0hka6hmyvp701adzag2g26cxdj47g21x6jz4sc6jjz1mn59d474y";
    };
  }
  {
    goPackagePath = "github.com/fatih/color";
    fetch = {
      type = "git";
      url = "https://github.com/fatih/color";
      rev = "v1.7.0";
      sha256 = "0v8msvg38r8d1iiq2i5r4xyfx0invhc941kjrsg5gzwvagv55inv";
    };
  }
  {
    goPackagePath = "github.com/go-test/deep";
    fetch = {
      type = "git";
      url = "https://github.com/go-test/deep";
      rev = "v1.0.3";
      sha256 = "0bpcrx505nc208ixjj3j65n23rfjkcgbnc4zjhh37rgqjm5yv6sz";
    };
  }
  {
    goPackagePath = "github.com/golang/glog";
    fetch = {
      type = "git";
      url = "https://github.com/golang/glog";
      rev = "23def4e6c14b";
      sha256 = "0jb2834rw5sykfr937fxi8hxi2zy80sj2bdn9b3jb4b26ksqng30";
    };
  }
  {
    goPackagePath = "github.com/golang/mock";
    fetch = {
      type = "git";
      url = "https://github.com/golang/mock";
      rev = "v1.3.1";
      sha256 = "1wnfa8njxdym1qb664dmfnkpm4pmqy22hqjlqpwaaiqhglb5g9d1";
    };
  }
  {
    goPackagePath = "github.com/golang/protobuf";
    fetch = {
      type = "git";
      url = "https://github.com/golang/protobuf";
      rev = "v1.3.2";
      sha256 = "1k1wb4zr0qbwgpvz9q5ws9zhlal8hq7dmq62pwxxriksayl6hzym";
    };
  }
  {
    goPackagePath = "github.com/golang/snappy";
    fetch = {
      type = "git";
      url = "https://github.com/golang/snappy";
      rev = "v0.0.1";
      sha256 = "0gp3kkzlm3wh37kgkhbqxq3zx07iqbgis5w9mf4d64h6vjq760is";
    };
  }
  {
    goPackagePath = "github.com/google/btree";
    fetch = {
      type = "git";
      url = "https://github.com/google/btree";
      rev = "v1.0.0";
      sha256 = "0ba430m9fbnagacp57krgidsyrgp3ycw5r7dj71brgp5r52g82p6";
    };
  }
  {
    goPackagePath = "github.com/google/go-cmp";
    fetch = {
      type = "git";
      url = "https://github.com/google/go-cmp";
      rev = "v0.3.1";
      sha256 = "1caw49i0plkjxir7kdf5qhwls3krqwfmi7g4h392rdfwi3kfahx1";
    };
  }
  {
    goPackagePath = "github.com/google/martian";
    fetch = {
      type = "git";
      url = "https://github.com/google/martian";
      rev = "v2.1.0";
      sha256 = "197hil6vrjk50b9wvwyzf61csid83whsjj6ik8mc9r2lryxlyyrp";
    };
  }
  {
    goPackagePath = "github.com/google/pprof";
    fetch = {
      type = "git";
      url = "https://github.com/google/pprof";
      rev = "54271f7e092f";
      sha256 = "14x4ydifz23rzaylggvwbm3dwlv1bc6s0bclmkxck9nbjbqw89vy";
    };
  }
  {
    goPackagePath = "github.com/google/uuid";
    fetch = {
      type = "git";
      url = "https://github.com/google/uuid";
      rev = "v1.1.1";
      sha256 = "0hfxcf9frkb57k6q0rdkrmnfs78ms21r1qfk9fhlqga2yh5xg8zb";
    };
  }
  {
    goPackagePath = "github.com/googleapis/gax-go";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/gax-go";
      rev = "v2.0.5";
      sha256 = "1lxawwngv6miaqd25s3ba0didfzylbwisd2nz7r4gmbmin6jsjrx";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/errwrap";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/errwrap";
      rev = "v1.0.0";
      sha256 = "0slfb6w3b61xz04r32bi0a1bygc82rjzhqkxj2si2074wynqnr1c";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-cleanhttp";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-cleanhttp";
      rev = "v0.5.1";
      sha256 = "07kx3fhryqmaw3czacmm11qwx63js2q8cfq967vphk7xg9q377kk";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-getter";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-getter";
      rev = "v1.4.0";
      sha256 = "0k9n19iacnjli42l905zhbib7kfil4k87ambnahcl5nand9maai0";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-hclog";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-hclog";
      rev = "v0.9.2";
      sha256 = "0pakba7rdkjgq50r79sbbpavymbyib77cy613wl734mpi30ywrxm";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-multierror";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-multierror";
      rev = "v1.0.0";
      sha256 = "00nyn8llqzbfm8aflr9kwsvpzi4kv8v45c141v88xskxp5xf6z49";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-plugin";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-plugin";
      rev = "v1.0.1";
      sha256 = "0aama8vdyrfzjdhxc1l4cwhmgydl989lywhq3pg3slzjg6r00rda";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-safetemp";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-safetemp";
      rev = "v1.0.0";
      sha256 = "0gydks8bkq88adlzmv8qj3rvljx15j94c8lyrp88ji2jn6dvv643";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-uuid";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-uuid";
      rev = "v1.0.1";
      sha256 = "0jvb88m0rq41bwgirsadgw7mnayl27av3gd2vqa3xvxp3fy0hp5k";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/go-version";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/go-version";
      rev = "v1.2.0";
      sha256 = "1bwi6y6111xq8ww8kjq0w1cmz15l1h9hb2id6596l8l0ag1vjj1z";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/golang-lru";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/golang-lru";
      rev = "v0.5.1";
      sha256 = "13f870cvk161bzjj6x41l45r5x9i1z9r2ymwmvm7768kg08zznpy";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/hcl";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/hcl";
      rev = "v1.0.0";
      sha256 = "0q6ml0qqs0yil76mpn4mdx4lp94id8vbv575qm60jzl1ijcl5i66";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/hcl/v2";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/hcl";
      rev = "v2.0.0";
      sha256 = "1ag764dx0s3j6s65jmiahcky675pwq0w96bml7dz5zzjfas4cdvp";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/logutils";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/logutils";
      rev = "v1.0.0";
      sha256 = "076wf4sh5p3f953ndqk1cc0x7jhmlqrxak9953rz79rcdw77rjvv";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/terraform-config-inspect";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/terraform-config-inspect";
      rev = "17f92b0546e8";
      sha256 = "1mnc4n4hrmyfx07xmwcawr2sxsjb92wi6qk6rayzvhsc6d8bmw5a";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/terraform-plugin-sdk";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/terraform-plugin-sdk";
      rev = "v1.6.0";
      sha256 = "1qhrr2wxl6gh1rsyvlxv0mxcrnn6bfl01avazpwj65f6a0i00vc4";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/terraform-svchost";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/terraform-svchost";
      rev = "65d371908596";
      sha256 = "160wjm1cm4x034kqxszlyjgxp3hmrwf7ll3slf13yw735j2cplq6";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/yamux";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/yamux";
      rev = "2f1d1f20f75d";
      sha256 = "1fga3p6j2g24ip9qjfwn3nqjr00m4nnjz92app7ms3sz7vgq2a7s";
    };
  }
  {
    goPackagePath = "github.com/jmespath/go-jmespath";
    fetch = {
      type = "git";
      url = "https://github.com/jmespath/go-jmespath";
      rev = "c2b33e8439af";
      sha256 = "1r6w7ydx8ydryxk3sfhzsk8m6f1nsik9jg3i1zhi69v4kfl4d5cz";
    };
  }
  {
    goPackagePath = "github.com/jstemmer/go-junit-report";
    fetch = {
      type = "git";
      url = "https://github.com/jstemmer/go-junit-report";
      rev = "af01ea7f8024";
      sha256 = "1lp3n94ris12hac02wi31f3whs88lcrzwgdg43a5j6cafg9p1d0s";
    };
  }
  {
    goPackagePath = "github.com/keybase/go-crypto";
    fetch = {
      type = "git";
      url = "https://github.com/keybase/go-crypto";
      rev = "93f5b35093ba";
      sha256 = "1wwnvdkfslcr6xy5m8kxypkli1a9g7qmbsq0rkl7qcmdcw9ypq7s";
    };
  }
  {
    goPackagePath = "github.com/kr/pretty";
    fetch = {
      type = "git";
      url = "https://github.com/kr/pretty";
      rev = "v0.1.0";
      sha256 = "18m4pwg2abd0j9cn5v3k2ksk9ig4vlwxmlw9rrglanziv9l967qp";
    };
  }
  {
    goPackagePath = "github.com/kr/pty";
    fetch = {
      type = "git";
      url = "https://github.com/kr/pty";
      rev = "v1.1.1";
      sha256 = "0383f0mb9kqjvncqrfpidsf8y6ns5zlrc91c6a74xpyxjwvzl2y6";
    };
  }
  {
    goPackagePath = "github.com/kr/text";
    fetch = {
      type = "git";
      url = "https://github.com/kr/text";
      rev = "v0.1.0";
      sha256 = "1gm5bsl01apvc84bw06hasawyqm4q84vx1pm32wr9jnd7a8vjgj1";
    };
  }
  {
    goPackagePath = "github.com/kylelemons/godebug";
    fetch = {
      type = "git";
      url = "https://github.com/kylelemons/godebug";
      rev = "v1.1.0";
      sha256 = "0dkk3friykg8p6wgqryx6745ahhb9z1j740k7px9dac6v5xjp78c";
    };
  }
  {
    goPackagePath = "github.com/mattn/go-colorable";
    fetch = {
      type = "git";
      url = "https://github.com/mattn/go-colorable";
      rev = "v0.1.1";
      sha256 = "0l640974j804c1yyjfgyxqlsivz0yrzmbql4mhcw2azryigkp08p";
    };
  }
  {
    goPackagePath = "github.com/mattn/go-isatty";
    fetch = {
      type = "git";
      url = "https://github.com/mattn/go-isatty";
      rev = "v0.0.5";
      sha256 = "114d5xm8rfxplzd7nxz97gfngb4bhqy102szl084d1afcxsvm4aa";
    };
  }
  {
    goPackagePath = "github.com/mattn/go-runewidth";
    fetch = {
      type = "git";
      url = "https://github.com/mattn/go-runewidth";
      rev = "v0.0.4";
      sha256 = "00b3ssm7wiqln3k54z2wcnxr3k3c7m1ybyhb9h8ixzbzspld0qzs";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/cli";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/cli";
      rev = "v1.0.0";
      sha256 = "1i9kmr7rcf10d2hji8h4247hmc0nbairv7a0q51393aw2h1bnwg2";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/colorstring";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/colorstring";
      rev = "d06e56a500db";
      sha256 = "1d2mi5ziszfzdgaz8dg4b6sxa63nw1jnsvffacqxky6yz9m623kn";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/copystructure";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/copystructure";
      rev = "v1.0.0";
      sha256 = "05njg92w1088v4yl0js0zdrpfq6k37i9j14mxkr3p90p5yd9rrrr";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/go-homedir";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/go-homedir";
      rev = "v1.1.0";
      sha256 = "0ydzkipf28hwj2bfxqmwlww47khyk6d152xax4bnyh60f4lq3nx1";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/go-testing-interface";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/go-testing-interface";
      rev = "v1.0.0";
      sha256 = "1dl2js8di858bawg7dadlf1qjpkl2g3apziihjyf5imri3znyfpw";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/go-wordwrap";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/go-wordwrap";
      rev = "v1.0.0";
      sha256 = "1jffbwcr3nnq6c12c5856bwzv2nxjzqk3jwgvxkwi1xhpd2by0bf";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/mapstructure";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/mapstructure";
      rev = "v1.1.2";
      sha256 = "03bpv28jz9zhn4947saqwi328ydj7f6g6pf1m2d4m5zdh5jlfkrr";
    };
  }
  {
    goPackagePath = "github.com/mitchellh/reflectwalk";
    fetch = {
      type = "git";
      url = "https://github.com/mitchellh/reflectwalk";
      rev = "v1.0.1";
      sha256 = "0pa6a3nhzwv5s5yqcmsmsfhdp5ggxsg2wa86f3akawxrhrkjarnx";
    };
  }
  {
    goPackagePath = "github.com/oklog/run";
    fetch = {
      type = "git";
      url = "https://github.com/oklog/run";
      rev = "v1.0.0";
      sha256 = "1pbjza4claaj95fpqvvfrysvs10y7dm0pl6qr5lzh6qy1vnhmcgw";
    };
  }
  {
    goPackagePath = "github.com/pierrec/lz4";
    fetch = {
      type = "git";
      url = "https://github.com/pierrec/lz4";
      rev = "v2.0.5";
      sha256 = "0y5rh7z01zycd59nnjpkqq0ydyjmcg9j1xw15q1i600l9j9g617p";
    };
  }
  {
    goPackagePath = "github.com/pmezard/go-difflib";
    fetch = {
      type = "git";
      url = "https://github.com/pmezard/go-difflib";
      rev = "v1.0.0";
      sha256 = "0c1cn55m4rypmscgf0rrb88pn58j3ysvc2d0432dp3c6fqg6cnzw";
    };
  }
  {
    goPackagePath = "github.com/posener/complete";
    fetch = {
      type = "git";
      url = "https://github.com/posener/complete";
      rev = "v1.2.1";
      sha256 = "0frf3a9dczs6fxysm2lm5lv9ch69ymnq1ds910x91q0lqdx723vp";
    };
  }
  {
    goPackagePath = "github.com/sergi/go-diff";
    fetch = {
      type = "git";
      url = "https://github.com/sergi/go-diff";
      rev = "v1.0.0";
      sha256 = "0swiazj8wphs2zmk1qgq75xza6m19snif94h2m6fi8dqkwqdl7c7";
    };
  }
  {
    goPackagePath = "github.com/spf13/afero";
    fetch = {
      type = "git";
      url = "https://github.com/spf13/afero";
      rev = "v1.2.2";
      sha256 = "0j9r65qgd58324m85lkl49vk9dgwd62g7dwvkfcm3k6i9dc555a9";
    };
  }
  {
    goPackagePath = "github.com/spf13/pflag";
    fetch = {
      type = "git";
      url = "https://github.com/spf13/pflag";
      rev = "v1.0.3";
      sha256 = "1cj3cjm7d3zk0mf1xdybh0jywkbbw7a6yr3y22x9sis31scprswd";
    };
  }
  {
    goPackagePath = "github.com/stretchr/objx";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/objx";
      rev = "v0.1.0";
      sha256 = "19ynspzjdynbi85xw06mh8ad5j0qa1vryvxjgvbnyrr8rbm4vd8w";
    };
  }
  {
    goPackagePath = "github.com/stretchr/testify";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/testify";
      rev = "v1.3.0";
      sha256 = "0wjchp2c8xbgcbbq32w3kvblk6q6yn533g78nxl6iskq6y95lxsy";
    };
  }
  {
    goPackagePath = "github.com/ulikunitz/xz";
    fetch = {
      type = "git";
      url = "https://github.com/ulikunitz/xz";
      rev = "v0.5.5";
      sha256 = "07mivr4aiw3b8qzwajsxyjlpbkf3my4xx23lv0yryc4pciam5lhy";
    };
  }
  {
    goPackagePath = "github.com/vmihailenco/msgpack";
    fetch = {
      type = "git";
      url = "https://github.com/vmihailenco/msgpack";
      rev = "v4.0.1";
      sha256 = "0faxbfbyy40rdc43np0kw53qzgigz0i44xkzvnslpniw16albrm4";
    };
  }
  {
    goPackagePath = "github.com/zclconf/go-cty";
    fetch = {
      type = "git";
      url = "https://github.com/zclconf/go-cty";
      rev = "v1.1.0";
      sha256 = "1sf90qk0q86dz1pjp7kwyw4xm12xd5x0b7j12xza6n591qj6jz7q";
    };
  }
  {
    goPackagePath = "github.com/zclconf/go-cty-yaml";
    fetch = {
      type = "git";
      url = "https://github.com/zclconf/go-cty-yaml";
      rev = "v1.0.1";
      sha256 = "0dams5g61n88rk7zq7sy0yap873ksjafhf81hn2fg2dpfjhcd3y2";
    };
  }
  {
    goPackagePath = "go.opencensus.io";
    fetch = {
      type = "git";
      url = "https://github.com/census-instrumentation/opencensus-go";
      rev = "v0.22.0";
      sha256 = "05jr8gkr2w34i5wwki4zhl5ch0qrgi7cdgag5iy5gpxplhbrvbg9";
    };
  }
  {
    goPackagePath = "golang.org/x/crypto";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/crypto";
      rev = "60c769a6c586";
      sha256 = "1wy2pg38dz29vf1h48yfqf8m3jqvwnbdw8vkk3ldlj5d8fbbbmv8";
    };
  }
  {
    goPackagePath = "golang.org/x/exp";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/exp";
      rev = "efd6b22b2522";
      sha256 = "0ysahwb7p6y09izks4ca8nk2w414gmjxzz44l5rmadlvk3k66cgp";
    };
  }
  {
    goPackagePath = "golang.org/x/image";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/image";
      rev = "0694c2d4d067";
      sha256 = "0v4rs4xpi7agbdzjw713mp7gzij8z89058s0yfj3276mzlns3zk4";
    };
  }
  {
    goPackagePath = "golang.org/x/lint";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/lint";
      rev = "959b441ac422";
      sha256 = "1mgcv5f00pkzsbwnq2y7vqvd1b4lr5a3s47cphh2qv4indfk7pck";
    };
  }
  {
    goPackagePath = "golang.org/x/mobile";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/mobile";
      rev = "d3739f865fa6";
      sha256 = "079ck2dyikacnph9s5mf0hrjnqlk6lc8q64dwnyw45w3xbbc50mg";
    };
  }
  {
    goPackagePath = "golang.org/x/net";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/net";
      rev = "d66e71096ffb";
      sha256 = "1dgjf0b0pzjw34zqkmy90552fdkwbnfn217r6zr903hh5dmd92nd";
    };
  }
  {
    goPackagePath = "golang.org/x/oauth2";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/oauth2";
      rev = "0f29369cfe45";
      sha256 = "06jwpvx0x2gjn2y959drbcir5kd7vg87k0r1216abk6rrdzzrzi2";
    };
  }
  {
    goPackagePath = "golang.org/x/sync";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sync";
      rev = "112230192c58";
      sha256 = "05i2k43j2d0llq768hg5pf3hb2yhfzp9la1w5wp0rsnnzblr0lfn";
    };
  }
  {
    goPackagePath = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev = "51ab0e2deafa";
      sha256 = "0xdhpckbql3bsqkpc2k5b1cpnq3q1qjqjjq2j3p707rfwb8nm91a";
    };
  }
  {
    goPackagePath = "golang.org/x/text";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/text";
      rev = "v0.3.2";
      sha256 = "0flv9idw0jm5nm8lx25xqanbkqgfiym6619w575p7nrdh0riqwqh";
    };
  }
  {
    goPackagePath = "golang.org/x/time";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/time";
      rev = "9d24e82272b4";
      sha256 = "1f5nkr4vys2vbd8wrwyiq2f5wcaahhpxmia85d1gshcbqjqf8dkb";
    };
  }
  {
    goPackagePath = "golang.org/x/tools";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/tools";
      rev = "6cdbf07be9d0";
      sha256 = "08nsarz53xbapsbdrj4kp05rli27zx6bmyb9f52hd739yi2375pv";
    };
  }
  {
    goPackagePath = "google.golang.org/api";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-api-go-client";
      rev = "v0.9.0";
      sha256 = "1lzdzkd2i41v6amxs9jah1q44qbvf1yvm8906jpfjiq6c3ffhqss";
    };
  }
  {
    goPackagePath = "google.golang.org/appengine";
    fetch = {
      type = "git";
      url = "https://github.com/golang/appengine";
      rev = "v1.6.1";
      sha256 = "0zxlvwzxwkwz4bs4h9zc9979dx76y4xf9ks4d22bclg47dv59yry";
    };
  }
  {
    goPackagePath = "google.golang.org/genproto";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/go-genproto";
      rev = "24fa4b261c55";
      sha256 = "109zhaqlfd8zkbr1hk6zqbs6vcxfrk64scjwh2nswph05gr0m84d";
    };
  }
  {
    goPackagePath = "google.golang.org/grpc";
    fetch = {
      type = "git";
      url = "https://github.com/grpc/grpc-go";
      rev = "v1.23.0";
      sha256 = "1cn33r2gclmq2v1ndpf1n5bmhf2qs8mms7ii5cnl6f9ch4r2c4k3";
    };
  }
  {
    goPackagePath = "gopkg.in/check.v1";
    fetch = {
      type = "git";
      url = "https://gopkg.in/check.v1";
      rev = "788fd7840127";
      sha256 = "0v3bim0j375z81zrpr5qv42knqs0y2qv2vkjiqi5axvb78slki1a";
    };
  }
  {
    goPackagePath = "gopkg.in/cheggaaa/pb.v1";
    fetch = {
      type = "git";
      url = "https://gopkg.in/cheggaaa/pb.v1";
      rev = "v1.0.27";
      sha256 = "1f995nsqgi0r4qc16nmxlbdxc536hkfwa5k9xb8ypz8d6jlkihag";
    };
  }
  {
    goPackagePath = "honnef.co/go/tools";
    fetch = {
      type = "git";
      url = "https://github.com/dominikh/go-tools";
      rev = "ea95bdfd59fc";
      sha256 = "1763nw7pwpzkvzfnm63dgzcgbq9hwmq5l1nffchnhh77vgkaq4ic";
    };
  }
  {
    goPackagePath = "rsc.io/binaryregexp";
    fetch = {
      type = "git";
      url = "https://github.com/rsc/binaryregexp";
      rev = "v0.2.0";
      sha256 = "1kar0myy85waw418zslviwx8846zj0m9cmqkxjx0fvgjdi70nc4b";
    };
  }
]
